#Write a function that takes a list of words and groups the words by their length using a dictionary

from collections import defaultdict


words = ['Lorem', 'ele', 'sit', 'incididunt', 'a', 'su', 'dom', 'ouch', 'ipsum' ]

d = defaultdict(list)

for word in words:

    d[len(word)].append(word)

print(d)


