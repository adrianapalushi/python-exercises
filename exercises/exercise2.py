# write a function to check if a number is a palindrome

def checkPalindrome(number):
    converted_number = str(number)
    reverse_string = converted_number[::-1]
    return f" {number} is a palindrome number" if reverse_string == converted_number else f" {number} is not"


number = int(input("Please insert a number?:"))
print(checkPalindrome(number))
