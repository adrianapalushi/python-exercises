#Create a class Person with first_name and last_name as constructor parameters.
# The class should have a function get_fullname which returns the person’s full name (first_name + last_name).
# Create another class Student, inheriting from Person.
# Student’s get_fullname function should return students full name followed by ‘-st.’


class Person:
  def __init__(self, first_name= "Nina", last_name="Dobrev"):
    self.firstname = first_name
    self.lastname = last_name

  @property
  def get_fullname(self):
    return f"{self.firstname } {self.lastname}"


class Student(Person):
  def __str__(self):
    return f"-st.{self.get_fullname} "
print(Student())
