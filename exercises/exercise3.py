# number is power of 2 or not
# Function to check if x is power of 2

def isPowerOfTwo(number):
    if number == 0:
        return False
    while number != 1:
        if number % 2 != 0:
            return False
        number = number // 2

    return True

number=int(input("please insert a number?:"))
if isPowerOfTwo(number):
    print(f' {number} is power of 2')
else:
    print(f'{number} is not power of 2')
