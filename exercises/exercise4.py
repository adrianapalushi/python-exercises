#4Write a function to convert a given string to all uppercase
# if it contains at least 2 uppercase characters in the first 4 characters

def str_uppercase(letters):
    letter_upper = 0
    for letter in letters[:4]:
        if letter.upper() == letter:
            letter_upper += 1
    if letter_upper >= 2:
        return letters.upper()
    return letters


print(str_uppercase("ADRiana"))
print(str_uppercase("JuNior"))
print(str_uppercase("PYthon") )
print(str_uppercase("DeVeloper"))